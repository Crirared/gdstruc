#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	cout << "Enter size of array: ";
	int size;
	cin >> size;


	srand(time(NULL));
	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
	{
		cout << unordered[i] << "   ";
		cout << "Ordered: ";
		cout << "   " << endl;
	}

	for (int i = 0; i < ordered.getSize(); i++)
	{
		cout << ordered[i] << "   ";
	}

	cout << "\n\nEnter number to search: ";
	int input;
	cin >> input;
	cout << endl;

	cout << "Unordered Array(Linear Search):\n";
	int result = unordered.linearSearch(input);
	if (result >= 0)
		cout << input << " was found at index " << result << ".\n";
	else
		cout << input << " not found." << endl;


	cout << "Ordered Array(Binary Search):\n";


	cout << "Ordered Array(Binary Search):\n";
	result = ordered.binarySearch(input);
	if (result >= 0)
		cout << input << " was found at index " << result << ".\n";
	else
		cout << input << " not found." << endl;

	//remove
	cout << "What would you like to do" << endl;
	int userInput;
	cin >> userInput;
	cout << "Remove" << endl;
	cout << " Search" << endl;
	cout << "Add a Number" << endl;
	switch ( userInput)
	{
		case 1: cout << "Remove Number" << endl;
		{
			cout << "which index would you like to remove" << endl;
			cin >> userInput;
			unordered.remove(userInput);
			ordered.remove(userInput);
			cout << "\nGenerated array: " << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "Ordered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			cout << "   " << endl;

			break;

		}

		case 2:cout << "Search Number" << endl;
		{
			cin >> userInput;
			unordered.linearSearch(userInput);
			ordered.binarySearch(userInput);
		

			break;
		}

		case 3:cout << "Add a Number" << endl;
		{
			cin >> size;
			unordered.push(size);
			ordered.push(size);
			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			for (int i = 0; i < unordered.getSize(); i++)
			{
				cout << unordered[i] << "   ";
				cout << "Ordered: ";
				cout << "   " << endl;
			}
			for (int i = 0; i < ordered.getSize(); i++)
			{
				cout << ordered[i] << "   ";
			}

			break;
		}
	
	}

	system("pause");
}