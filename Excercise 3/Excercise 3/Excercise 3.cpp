#include <iostream>

using namespace std;

int sum(int number)
{
	if (number == 0)
		return 0;
	else
		return (number % 10 + sum(number / 10));
}

int fibonacci(int n)
{
	if (n <= 1)
		return n;
	return(fibonacci(n - 1) + fibonacci(n - 2));
}

bool prime(int x, int i = 2)
{
	if (x <= 2)
		return (x == 2) ? true : false;
	if (x % i == 0)
		return false;
	if (i * i > x)
		return true;

	return prime(x, i + 1);
}

int main()
{
	//compute the sum of digits of a number 
	int n;
	int m;
	int x;
	cout << "Enter number till which you want the sum of natural numbers to be calculated";
	cout << endl;
	cin >> n;
	int total = sum(n);
	cout << endl << "Sum of natural numbers from  to " << n << " are: " << total << endl;


	//print the fibonacci sequence up to n
	cout << "print the fibonacci sequence up to n" << endl;
	cin >> m;
	cout << "The Fibonacci number is : " << fibonacci(m) << endl;
	getchar();
	cout << " Input the number to check if its prime?" << endl;
	cin >> x;
	if (prime(x))
		cout << " Yes";
	else
		cout << "No";
	return 0;

}